#include<bits/stdc++.h>
using namespace std;

int main()
{
    vector<int> tempature = {73,74,75,71,69,72,76,73};
    vector<int> ans1(tempature.size(), -1), ans2(tempature.size(), -1), ans3(tempature.size());
    stack<pair<int, int>> stk;

    for (int i = tempature.size()-1; i >= 0; i--)
    {
        while (!stk.empty() && tempature[i] >= stk.top().first)
        {
            stk.pop();
        }

        if (stk.empty())
            ans3[i] = 0;
        else
        {
            ans1[i] = stk.top().first;
            ans2[i] = stk.top().second;
            ans3[i] = stk.top().second - i;
        }

        stk.push({tempature[i], i});
    }

    printf("Next greater element\n");
    for (int i = 0; i < ans1.size(); i++)
        cout << ans1[i] << " ";

    printf("Next greater element index\n");
    for (int i = 0; i < ans2.size(); i++)
        cout << ans2[i] << " ";

    printf("Next greater element distance\n");
    for (int i = 0; i < ans3.size(); i++)
        cout << ans3[i] << " ";

}