#include <bits/stdc++.h>
using namespace std;

#define ar array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void display(stack<int> stk)
{
    while (!stk.empty())
    {
        cout << stk.top() <<endl;
        stk.pop();
    }
}

void reversingStack(stack<int>& stk)
{
    if(stk.empty() == true)
        return;

    int temp_top = stk.top();
    stk.pop();
    reversingStack(stk);

    stack<int> temp_stk;

    while(!stk.empty())
    {
        temp_stk.push(stk.top());
        stk.pop();
    }
    
    stk.push(temp_top);
    while(!temp_stk.empty())
    {
        stk.push(temp_stk.top());
        temp_stk.pop();
    }
}

void solve() 
{
    stack<int> stk;

    stk.push(1);
    stk.push(2);
    stk.push(3);
    stk.push(4);
    stk.push(5);
    stk.push(6);
    stk.push(7);

    printf("\nCurrent Stack is\n");
    display(stk);

    reversingStack(stk);

    printf("\nAfter reversing, Stack is\n");
    display(stk);
}


int main() 
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) 
    {
        // cout << "Case #" << t << ": ";
        solve();
    }
}
