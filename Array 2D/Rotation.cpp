/*
What is the output if we rotate a 2D array by 90 degree?
*/

#include <bits/stdc++.h>
using namespace std;
int main()
{
    vector<vector<int>> matrix = {{1,2,3}, {4,5,6}, {7,8,9}};

    // printing each elements
    for(int col = 0; col < matrix[0].size(); col++)
    {
      for(int row = matrix.size()-1; row >= 0; row--)
      {
        cout << matrix[row][col] << " ";
      }
      cout << endl;
    }


    // printing a 90 degree rotated matrix
    vector<vector<int>> matrix2(n, vector<int>(m));

    for (int col = 0; col < n; col++)
    {
        for (int row = m - 1; row >= 0; row--)
        {
            matrix2[col][m - 1 - row] = matrix[row][col];
        }
    }

    // Display the rotated matrix
    for (int i = 0; i < matrix2.size(); i++)
    {
        for (int j = 0; j < matrix2[0].size(); j++)
        {
            cout << matrix2[i][j] << " ";
        }
        cout << endl;
    }
}

/*
Output:

7 4 1 
8 5 2 
9 6 3 
*/