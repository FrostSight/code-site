#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> approach1_construct2DArray(vector<int>& original, int m, int n) 
{
    vector<vector<int>> result (m, vector<int> (n));

    if(original.size() != m*n)  return {};  // corner case

    for(int i = 0; i < original.size(); i++)  // iterating over the given 1D array
    {
        int row = i / n; // getting row
        int col = i % n; // getting column
        result[row][col] = original[i];
    }

    return result;
}

vector<vector<int>> approach2_construct2DArray(vector<int>& original, int m, int n) 
    {
        vector<vector<int>> result(m, vector<int>(n));
        int l = original.size();

        if(original.size() != m*n)  return {};

        int idx = 0;

        for (int i = 0; i < m; i++) 
        {
            for (int j = 0; j < n; j++) 
            {
                result[i][j] = original[idx];
                idx++;
            }
        }

        return result;
    }



int main() 
{
    vector<int> original = {1, 2, 3, 4, 5, 6};
    int m = 2, n = 3;

    vector<vector<int>> result1 = approach1_construct2DArray(original, m, n);

    cout << "The constructed 2D array is:" << endl;
    for (auto& row : result1) 
    {
        for (int val : row) 
        {
            cout << val << " ";
        }
        cout << endl;
    }
    vector<vector<int>> result2 = approach2_construct2DArray(original, m, n);

    cout << "The constructed 2D array is:" << endl;
    for (auto& row : result2) 
    {
        for (int val : row) 
        {
            cout << val << " ";
        }
        cout << endl;
    }

}
