#include<stdio.h>
#include<math.h>
int main()
{
    FILE *f1, * f2;
    char c;
    char flag = '=';

    f1 = fopen("Test1.c", "r");
    f2 = fopen("Testing.txt", "w");

    if (!f1)
        printf("File can not be opened\n");
    else
    {
        while((c = fgetc(f1)) != EOF)
        {

            if (c == '<')
            {
                flag = '<' ;
                continue;
            }
            else if (c == '>')
            {
                flag = '=';
                fputc('\n', f2);
            }
            else
            {
                if (flag == '<')
                    fputc(c, f2);
            }
        }
    }

    fclose(f1);
    fclose(f2);

    f2 = fopen("Testing.txt", "r");

    while((c = fgetc(f2)) != EOF)
        printf("%c", c);

    fclose(f2);
}
