#include <bits/stdc++.h>
using namespace std;

int n;
double mult;
double x[100], y[100][100],approx_res[100],sum_x=0,sum_y=0,true_res[100];

double productTerm(int i,double value)
{
    double pro = 1;
    for (int j = 0; j < i; j++)
    {
        pro = pro * (value - x[j]);
    }
    return pro;
}

void dividedDiffTable()
{
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < n - i; j++)
        {
            y[j][i] = (y[j][i - 1] - y[j + 1][i - 1]) / (x[j] - x[i + j]);
        }
    }
}

double applyNewtonDivideFormula(double value)
{
    double sum = y[0][0];
    for (int i = 1; i < n; i++)
    {
      sum = sum + (productTerm(i, value) * y[0][i]);
    }
    return sum;
}


double applyLagrangeInterPolationFormula(double value)
{
    double sum = 0;

     for(int i=0;i<=n-1;i++)
    {
        mult=1;
        for(int j=0;j<=n-1;j++)
        {
            if(j!=i)
                mult*=(value-x[j])/(x[i]-x[j]);
        }
        sum+=mult*y[i][0];
    }

    return sum;
}

int main()
{
    cout<<"Enter number of data points: ";
    cin>>n;
    cout<<"Enter values of X and cube root of X "<<endl;
    for(int i=0;i<n;i++)
    {
        cin>>x[i]>>y[i][0];
    }

    cout<<"x         Interpolated cube root of x         True Value of 3√x      Absolute Error  "<<endl;

    double approx_x,true_x;
    double value=1.15,h=0.05;
    while(value<=1.65)
    {
        approx_x = applyLagrangeInterPolationFormula(value);
        true_x = cbrt(value);
        double e = fabs(true_x - approx_x);
        cout<<value<<"          "<<approx_x<<"           "<<true_x<<"             "<<e<<endl;
        
        value=value+h;
    }

    cout<<endl;
    cout<<endl;

//--------------------------------------------------------------------------------------------------------------------------------------------

    dividedDiffTable();
    
    cout<<"x         Interpolated cube root of x         True Value of 3√x      Absolute Error  "<<endl;

    value=1.15,h=0.05;
    while(value<=1.65)
    {
        approx_x = applyNewtonDivideFormula(value);
        true_x = cbrt(value);
        double e = fabs(true_x - approx_x);
        cout<<value<<"          "<<approx_x<<"          "<<true_x<<"            "<<e<<endl;
        
        value=value+h;
    }
   
    cout<<endl;
    cout<<endl;
}
