#include <bits/stdc++.h>

int a[10][10], b[10], c, n;

void jordan()
{
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (i != j)
            {
                c = a[j][i] / a[i][i];
                for (int k = 1; k <= n + 1; k++)
                {
                    a[j][k] = a[j][k] - c * a[i][k];
                }
            }
        }
    }
    for (int i = 1; i <= n; i++)
    {
        b[i] = a[i][n + 1] / a[i][i];
    }
    printf("Starting of Execution Gauss Jordan Method:\nThe solution of linear equations is:The solution of linear equations is:\n");
    for (int i = 1; i <= n; i++)
    {
        printf("b[%d] = %d\n", i, b[i]);
    }
    printf("End of Execution......\n");
}

int main()
{
    printf("Enter the size of the equations: ");
    scanf("%d", &n);

    printf("Enter the elements of Coefficients:\n");
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n + 1; j++)
        {
            scanf("%d", &a[i][j]);
        }
    }

    double timee = 0.0;
    clock_t starting = clock();
    jordan();
    clock_t finishing = clock();
    timee = timee + (double)(finishing - starting) / CLOCKS_PER_SEC;
    printf("\nRunning Time for Gauss Jordan Method is %lf\n", timee);
}
