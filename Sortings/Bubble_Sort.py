def bubble_sorting(data_list):
    step_counter = 0
    sub_step_counter = 0
    n = len(data_list)
    
    for i in range(n):
        step_counter += 1
        swapped = False
        
        for j in range(n - i - 1):
            if data_list[j] > data_list[j + 1]:
                data_list[j], data_list[j + 1] = data_list[j + 1], data_list[j]
                swapped = True
            
            sub_step_counter += 1
            print(f"Step {step_counter} sub-step {sub_step_counter}: ", end="")
            for k in range(n):
                print(f"{data_list[k]} ", end="")
            
            print()
    
        if swapped == False:
            print("\nGiven array is already sorted.\n")
            break
    
        sub_step_counter = 0

    return data_list


list_size = int(input("Enter list size "))

data_list_1 = []
print("Enter list elements")
for i in range(list_size):
    i = int(input())
    data_list_1.append(i)

print("\nYou have provided", data_list_1) #before

data_list_2 = bubble_sorting(data_list_1)

print("\nSelection sort is performed") #after
print(data_list_2)
