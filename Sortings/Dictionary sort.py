# <----------Dictionary Sort---------->
dict1 = {'apple': 10, 'orange': 20, 'banana': 5, 'rotten tomato': 1, 'grape': 15, 'kiwi': 3, 'mango': 7, 'pear': 12, 'pineapple': 2, 'watermelon': 8}
print ("Original\n", dict1)

print("\nSorted based on keys:")
# by default sort function returns a list of tuple
dict2 = dict(sorted(dict1.items())) # type casting to dictionary
print (dict2)

print("\nSorted based on values:")
dict3 = dict(sorted(dict1.items(), key = lambda x: x[1]))
print(dict3)

# <----------List of Dictionary Sort---------->
dict4 = [{'make':'Nokia', 'model':216, 'color':'Black'}, {'make':'Mi Max', 'model':'2', 'color':'Gold'}, {'make':'Samsung', 'model': 7, 'color':'Blue'}]
print("Original:")
print(dict1)
lis = sorted(dict1, key = lambda x: x['color']) # list output
# color(key) er value lexographically sort kore
print("\nSort based on color:")
print(lis)

# x[0] x[1] hoy na 
# output dictionary te convert kora jay na