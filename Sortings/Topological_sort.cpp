#include <bits/stdc++.h>
using namespace std;

vector<int> tree[10], res;
bool found[10];

// DFS method
void visiting(int head)
{
    found[head] = 1;
    for (int i = 0; i < tree[head].size(); i++)
    {
        int next = tree[head][i];
        if (found[next] == 0)
            visiting(next);
    }
    res.push_back(head);
}

int main()
{
    int nodes, edges;
    printf("Enter number of nodes ");
    scanf("%d", &nodes);
    printf("Enter number of edges ");
    scanf("%d", &edges);
    printf("Enter elements\n");
    for (int i = 0; i < edges; i++)
    {
        int u, v;
        scanf("%d%d", &u, &v);
        tree[u].push_back(v);
    }

    for (int i = 0; i < nodes; i++)
    {
        if (found[i] == 0)
            visiting(i);
    }

    reverse(res.begin(), res.end());

    printf("\nThe sort is\n");
    for (int i = 0; i < res.size() - 1; i++)
        printf("%d ", res[i]);
}
