#include<bits/stdc++.h>
using namespace std;


void bubble_sort(int arr[], int n)
{
    int step_counter = 0;
    int sub_step_counter = 0;

    for (int i = 0; i < n; i++)
    {
        step_counter++;
        bool swapped = false;  // check if array is already sorted

        for (int j = 0; j < (n - i) - 1; j++)
        {
            if(arr[j] > arr[j + 1])
            {
                swap(arr[j], arr[j + 1]);
                swapped = true;
            }

                sub_step_counter++;  // from 23 to 36 (except 30 to 34) are for decoration
                cout << "Step " << step_counter << " sub-step " << sub_step_counter << ": ";
                for (int k = 0; k < n; k++)
                    cout << arr[k] << " ";

                printf("\n");
        }
        if(swapped == false) // array is already sorted and the best case
        {
            printf("\nGiven array is already sorted.\n");
            break;
        }

        sub_step_counter = 0;
    }
}


int main()
{
    int arr_size;
    printf("Enter array size ");
    scanf("%d", &arr_size);

    int arr[arr_size];
    printf("Enter elements\n");
    for (int i = 0; i < arr_size; i++)
    {
        scanf("%d", &arr[i]);
    }

    printf("\nBefore sorting\n");
    for (int i = 0; i < arr_size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");

    bubble_sort(arr, arr_size);  // I directly pass the array size otherwise  it creates a bug

    printf("\nAfter sorting\n");
    for (int i = 0; i < arr_size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}


/* Time Complexity
---------------------------------------
==> wrost case: Descending order O(n^2)
==> best case: Ascending order O(n)
---------------------------------------
*/
