def selection_sort(data_list):
    print("\nSimulation")
    
    for i in range(len(data_list) - 1): # last elementis automatically sorted 
        min_index = i  # consider current element is minimum
        
        for j in range(i + 1, len(data_list)): #  looking for the minimum element, every element towards i to the very last element
            if data_list[j] < data_list[min_index]:
                min_index = j
        
        print(data_list) # printing every step
        data_list[i], data_list[min_index] = data_list[min_index], data_list[i] # swapping current with minimum element
    
    return data_list


list_size = int(input("Enter list size "))

data_list_1 = []
print("Enter list elements")
for i in range(list_size):
    i = int(input())
    data_list_1.append(i)

print("\nYou have provided", data_list_1) #before 

data_list_2 = selection_sort(data_list_1)

print("\nSelection sort is performed") #after
print(data_list_2)

