#include <bits/stdc++.h>
using namespace std;

int searching(int *arr, int size, int target)
{
  if(size == 0)
    return -1;
  
  if(arr[0] == target)
    return arr[0];
  else
    searching(arr+1, size-1, target);
}


int main() 
{

    int arr[8] = {1, 3, 4, 6, 7, 8, 9, 10};
    int target = 9;
    
    int val = searching(arr, 8, target);
    
    val == -1 ? cout << "target not found" << endl : cout << val << endl;
}
