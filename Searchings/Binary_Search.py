def searching(data_list, k):
    left = 0
    right = len(data_list) - 1
    
    while left <= right:
        mid = left + (right - left) // 2
        
        if k == data_list[mid]:
            return mid
        elif k < data_list[mid]:
            right = mid - 1
        else:
            left = mid + 1
    
    return -1


list_size = int(input("Enter list size "))

data_list = []
print("Enter list elements in acending order")
for i in range(list_size):
    i = int(input())
    data_list.append(i)

print("You have provided", data_list)

k = int(input("Enter the data you are looking for "))

rslt = searching(data_list, k)

if rslt != -1:
    print("\nData found at", rslt + 1, "position\n")
else:
    print("\nData not found\n")