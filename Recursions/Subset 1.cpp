#include <bits/stdc++.h>
using namespace std;

#define vi vector<int>

void subsets(int idx, vi& arr, vi& temp, vector<vector<int>>& ans)
{
    if(idx == arr.size())
    {
        ans.push_back(temp);
        return;
    }

    temp.push_back(arr[idx]);
    subsets(idx + 1, arr, temp, ans);
    temp.pop_back();
    subsets(idx + 1, arr, temp, ans);
}

void display(vector<vector<int>>& ans)
{
    for(int i = 0; i < ans.size(); i++)
    {
        for(int j = 0; j < ans[i].size(); j++)
        {
            cout << ans[i][j] << " ";
        }
        cout << endl;
    }
}

void solve() 
{
    // input
    vi arr = {1, 2, 3, 4, 5}, temp;
    vector<vector<int>> ans;

    // logic
    subsets(0, arr, temp, ans);

    // output
    display(ans);
}


int main() 
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    while (tc--) 
    {
        solve();
    }
}
