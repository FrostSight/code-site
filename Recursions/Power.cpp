#include<bits/stdc++.h>
using namespace std;

int power(int n)
{
    int biggerPrb, smallerPrb;

    // base case
    if(n == 0)
        return 1;
    
    /*smallerPrb = power(n - 1);
    biggerPrb = 2 * smallerPrb;

    return biggerPrb;*/

    return 2*power(n - 1);
}

int main()
{
    int n , rslt;
    scanf("%d", &n);

    rslt = power(n);

    printf("%d\n", rslt);
}