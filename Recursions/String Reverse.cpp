#include<bits/stdc++.h>
using namespace std;

string withRecursion(string s, int start, int end)
{
    if (start > end) 
        return s;

    swap(s[start], s[end]);
    start++;
    end--;

    return withRecursion(s, start, end);  //  return na dile code print hoy na
}

string withoutRecursion(string s, int start, int end)
{
    while (start < end)
    {
        swap(s[start], s[end]);
        start++;
        end--;
    }
    return s;
}

int main()
{
    string s = "abcdefghijk", s1, s2;

    s1 = withoutRecursion(s, 0, s.size()-1);
    cout << "Without recursion: " << s1 << endl;

    s2 = withRecursion(s, 0, s.size()-1);
    cout << "With recursion: " << s2 << endl;
}
