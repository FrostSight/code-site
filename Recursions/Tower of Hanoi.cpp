#include <iostream>
using namespace std;

long long toh(int n, int from, int target, int aux)
{
    if (n == 1)  // base case
    {
        printf("move disk %d from rod %d to rod %d", n, from, target);
        printf("\n");
        return 1;
    }

    int counter = toh(n - 1, from, aux, target);

    printf("move disk %d from rod %d to rod %d", n, from, target);
    printf("\n");

    counter += 1;

    counter += toh(n - 1, aux, target, from);

    return counter;
}

int main()
{
    int n = 3, from = 1, target = 3, aux = 2;
    long long moves = toh(n, from, target, aux);
    cout << "Total moves: " << moves << endl;
}
