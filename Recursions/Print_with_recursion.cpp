#include<bits/stdc++.h>
using namespace std;

void printing(int n)
{
    // base case
    if(n == 0)
        return ;
    
    cout << n << endl;

    printing(n - 1);
}

int main()
{
    int n , rslt;
    
    printf("Give input ");
    scanf("%d", &n);

    printf("Output\n");
    printing(n);

}