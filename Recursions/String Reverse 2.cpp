#include <bits/stdc++.h>
using namespace std;

void solve(string& str, int i) 
{
    if(i == str.size()) return;

    solve(str, i+1);
    cout << str[i] << endl;
}


int main() 
{
    string str = "abcde";
    solve(str, 0);
}

/*
Output:
e
d
c
b
a
*/