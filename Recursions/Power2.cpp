#include <bits/stdc++.h>
using namespace std;

int optimizely(int a, int b)
{
  if(b == 0)
    return 1;
  
  if(b == 1)
    return a;
  
  int ans = optimizely(a, b/2);
  
  if(b % 2 == 0)
    return ans * ans;
  else
    return a * ans * ans;
}


int unoptimizely(int a, int b)  // b times recursive call
{

  if(b == 0)
    return 1;
    
  
  int ans = unoptimizely(a, b - 1);
  return ans * a;
}

int main() 
{
    int a = 2, b = 9;
    
    int ans1 = unoptimizely(a, b);
    cout << ans1 << endl;
    
    int ans2 = optimizely(a, b);
    cout << ans2 << endl;
}