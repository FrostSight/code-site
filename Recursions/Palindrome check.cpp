#include<bits/stdc++.h>
using namespace std;

bool isPalindrome(string str, int start, int end)
{
    if(start > end)
        return true;
    
    if(str[start] != str[end])
        return false;
    else
    {
        start++;
        end--;
        return isPalindrome(str, start, end);
    }
}

int main()
{
    string str = "madam";

    bool palindrome = isPalindrome(str, 0, str.size()-1);

    if(palindrome)
        cout << "It is a palindrome" << endl;
    else
        cout << "It is not a palindrome" << endl;
}