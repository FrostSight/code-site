#include <bits/stdc++.h>
using namespace std;

void permutation(vector<int>& arr, vector<int>& temp, unordered_set<int> seen, vector<vector<int>>& ans)
{
    if(temp.size() == arr.size())
    {
        ans.push_back(temp);
        return;
    }
    for(int i = 0; i < arr.size(); i++)
    {
        if(seen.find(arr[i]) == seen.end())
        {
            seen.insert(arr[i]);
            temp.push_back(arr[i]);
            permutation(arr, temp, seen, ans);
            seen.erase(arr[i]);
            temp.pop_back();
        }
    }
}

void display(vector<vector<int>>& ans)
{
    for(int i = 0; i < ans.size(); i++)
    {
        for(int j = 0; j < ans[0].size(); j++)
        {
            cout << ans[i][j] << " ";
        }
        cout << endl;
    }
}

void solve()
{
    //input part
    vector<int> arr = {1, 2, 3}, temp;
    vector<vector<int>> ans;
    unordered_set<int> seen;

    //logic part
    permutation(arr, temp, seen, ans);

    // output part
    display(ans);
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);

    int tc = 1;
    while (tc--)
    {
        solve();
    }
}
