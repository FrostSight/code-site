#include <bits/stdc++.h>
using namespace std;

void merging(int arr[], int left, int mid, int right)
{
    int n1 = mid - left + 1, n2 = right - mid;
    int left_side[n1], right_side[n2];

    int k = left;
    for(int i = 0; i < n1; i++)
    {
        left_side[i] = arr[k];
        k++;
    }

    for(int i = 0; i < n2; i++)
    {
        right_side[i] = arr[k];
        k++;
    }

    int i = 0, j = 0;
    k = left;

    while(i < n1 && j < n2)
    {
        if(left_side[i] <= right_side[j])
        {
            arr[k] = left_side[i];
            i++;
        }
        else
        {
            arr[k] = right_side[j];
            j++;
        }
        k++;
    }

    // leftover of left_side
    while(i < n1)
    {
        arr[k] = left_side[i];
        i++;
        k++;
    }
    // leftover of right_side
    while(j < n2)
    {
        arr[k] = right_side[j];
        j++;
        k++;
    }
}

void breaking(int arr[], int left, int right)
{
    if(left < right)
    {
        int mid = left + (right - left) / 2;
        breaking(arr, left, mid);
        breaking(arr, mid + 1, right);

        merging(arr, left, mid, right);
    }
}

int main()
{
    int n = 5;
    int arr[n];
    cout << "Enter the elements: ";
    for(int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }

    breaking(arr, 0, n - 1);

    cout << "Sorted array: ";
    for(int i = 0; i < n; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;

    return 0;
}

