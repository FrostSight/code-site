#include <bits/stdc++.h>
using namespace std;

bool isSorted(vector<int>& v1)
{
  // gives compile error
  
  int n = v1.size();
  
  // base case
  if(n == 0 || n == 1)
    return true;
  
  if(v1[0] > v1[1])
    return true;
  else
    return isSorted(v1.begin()+1, n-1);
  
}

bool isSorteD(int *arr, int size)
{
  int n = size;
  
  // base case
  if(n == 0 || n == 1)
    return true;
  
  if(arr[0] > arr[1])
    return false;
  else
    return isSorteD(arr + 1, n - 1);
  
}
 


int main() 
{
    vector<int> v1 = {1, 3, 4, 6, 7, 8, 9, 10};
    isSorted(v1) ? cout << "Sorted" << endl : cout << "Not Sorted" << endl;
    
    printf("\n");
    
    int arr[8] = {1, 3, 4, 6, 7, 8, 9, 10};
    isSorteD(arr, 8) ? cout << "Array Sorted" << endl : cout << "Array Not Sorted" << endl;
}
