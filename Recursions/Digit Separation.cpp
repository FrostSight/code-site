#include <bits/stdc++.h>
using namespace std;

void fun1(int n)
{
  int rem;
  
  while(n != 0)
  {
    rem = n % 10;
    cout << rem << " ";
    n = n / 10;
  }
}

void fun2(int n)
{
  if(n == 0)
    return;
  
  int rem = n % 10;
  cout << rem << " ";
  n = n / 10;
  fun2(n);
}

int main() 
{
    int n = 123;
    fun1(n);
    cout << endl;
    fun2(n);
}
