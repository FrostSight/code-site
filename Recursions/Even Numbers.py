def recursion(n):
  if n == 0:
    return
  
  recursion(n-2)
  print(n, "\n")

def mainFunction():
  recursion(10)

if __name__ == '__main__':
  mainFunction()

'''
Output:
This is exactly how recursion works. Recursion run until reaching terminating condition then starts printing

2 

4 

6 

8 

10 

'''