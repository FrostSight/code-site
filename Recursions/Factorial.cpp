#include<bits/stdc++.h>
using namespace std;

int factorial(int n)
{
    int biggerPrb, smallerPrb;

    // base case
    if(n == 0)
        return 1;
    
    /*smallerPrb = factorial(n - 1);
    biggerPrb = n * smallerPrb;

    return biggerPrb;*/

    return n * factorial(n - 1);
}

int main()
{
    int n = 5, rslt;
    rslt = factorial(n);

    printf("%d\n", rslt);
}