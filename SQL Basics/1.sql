-- MySQL
SELECT 
    CASE 
        WHEN num1 > num2 AND num1 > num3
            THEN num1
        WHEN num2 > num1 AND num2 > num3
            THEN num2
        ELSE num3
    END AS max_number
FROM
    (SELECT 10 AS num1, 20 AS num2, 15 AS num3) AS numbers;



-- PostgreSQL
SELECT 
    CASE 
        WHEN num1 >= num2 AND num1 >= num3 
            THEN num1
        WHEN num2 >= num1 AND num2 >= num3 
            THEN num2
        ELSE num3
    END AS max_number
FROM 
    (SELECT 10 AS num1, 20 AS num2, 15 AS num3) AS numbers;