//Unbound knapsack - if I can take the element one time, I can take it multiple time. In this case, number of total elements will not decrerase
#include <bits/stdc++.h>
using namespace std;

int bagWeight, totalElement;
int dp[12][12]; // it should be at least dp[n+1][w+1] or [bagWeight+1][totalElement+1]

int chill(int bagWeight, int totalElement, int weights[], int profits[])
{
    // base condition
    if (bagWeight == 0 || totalElement == 0) // it is best practice to use the valid conditions as none of them can never be negative
        return 0;

    // memoization
    if (dp[totalElement][bagWeight] != -1)
        return dp[totalElement][bagWeight];

    // actual recursive conditions
    if (weights[totalElement - 1] <= bagWeight)
    {
        // either I can take the element or not
        dp[totalElement][bagWeight] = max(profits[totalElement - 1] + chill(bagWeight - weights[totalElement - 1], totalElement, weights, profits), chill(bagWeight, totalElement - 1, weights, profits)); // which element to take?? which provides the maximum profit || max (I take it or I don't take it)
    }
    else // (totalElement > bagWeight)
    {
        dp[totalElement][bagWeight] = chill(bagWeight, totalElement - 1, weights, profits);
    }
}

int main()
{
    memset(dp, -1, sizeof(dp));

    printf("Enter the weight of the bag ");
    scanf("%d", &bagWeight);
    printf("Enter the number of elements ");
    scanf("%d", &totalElement);

    int weights[totalElement], profits[totalElement];

    printf("Enter the weights of the elements ");
    for (int i = 0; i < totalElement; i++)
        scanf("%d", &weights[i]);

    printf("Enter the profits of the elements ");
    for (int i = 0; i < totalElement; i++)
        scanf("%d", &profits[i]);

    cout<<chill(bagWeight, totalElement, weights, profits)<<endl;
}

