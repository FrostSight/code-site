#include <bits/stdc++.h>
using namespace std;
#define INF INT_MAX

int P[100];
int s[100][100];
int m[100][100];

void INIT_MCM(int n)
{
    for (int i = 0; i < n; ++i)
    {
        m[i][i] = 0;
    }

    for (int l = 2; l < n; ++l)
    {
        for (int i = 1; i <= n - l; ++i)
        {
            int j = i + l - 1;
            m[i][j] = INF;
            printf("m(%d, %d) = min{", i, j);
            for (int k = i; k <= j - 1; ++k)
            {
                printf("m(%d, %d) + m(%d, %d) + P(%d)P(%d)P(%d)", i, k, k + 1, j, i - 1, k, j);
                if (k != j - 1)
                {
                    printf(",\t");
                }
            }
            printf("}\n");
            printf("        = min{");
            for (int k = i; k <= j - 1; ++k)
            {
                printf("%d + %d + %dx%dx%d", m[i][k], m[k + 1][j], P[i - 1], P[k], P[j]);
                if (k != j - 1)
                {
                    printf(",\t");
                }
            }
            printf("}\n");
            printf("        = min{");
            for (int k = i; k <= j - 1; ++k)
            {
                int curr = m[i][k] + m[k + 1][j] + (P[i - 1] * P[k] * P[j]);
                printf("%d", curr);
                if (k != j - 1)
                {
                    printf(",\t");
                }
                if (curr < m[i][j])
                {
                    m[i][j] = curr;
                    s[i][j] = k;
                }
            }
            printf("}\n");
            printf("m(%d, %d) = %d\n", i, j, m[i][j]);
            printf("s(%d, %d) = %d\n", i, j, s[i][j]);
        }
    }
}

void CHAIN_MCM(int i, int j)
{
    if (i == j)
    {
        cout << "A" << i;
    }
    else
    {
        cout << '(';
        CHAIN_MCM(i, s[i][j]);
        CHAIN_MCM(s[i][j] + 1, j);
        cout << ')';
    }
}

int main()
{
    int n;
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        cin >> P[i];
    }
    INIT_MCM(n);
    cout << endl;
    for (int i = 1; i < n; ++i)
    {
        for (int j = 1; j < n; ++j)
        {
            if (i > j)
            {
                cout << "  ";
            }
            else
            {
                cout << m[i][j] << " ";
            }
        }
        cout << endl;
    }
    cout << endl;
    for (int i = 1; i < n; ++i)
    {
        for (int j = 1; j < n; ++j)
        {
            if (i > j)
            {
                cout << "  ";
            }
            else
            {
                cout << s[i][j] << " ";
            }
        }
        cout << endl;
    }
    cout << endl;

    CHAIN_MCM(1, n - 1);
}

