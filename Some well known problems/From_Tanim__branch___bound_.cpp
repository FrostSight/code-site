#include<bits/stdc++.h>
#define N 5
#define INF INT_MAX
using namespace std;

struct Node {
    vector<pair<int, int>> path;
    int reduced_matrix[N][N];
    int cost;
    int current;
    int visited;
};

void row_reduce(int reduced_matrix[N][N], int min_row[N]) {

    for(int i = 0; i < N; i++)
        min_row[i] = INF;

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if (reduced_matrix[i][j] < min_row[i])
                min_row[i] = reduced_matrix[i][j];

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if (reduced_matrix[i][j] != INF && min_row[i] != INF)
                reduced_matrix[i][j] -= min_row[i];
}

void column_reduce(int reduced_matrix[N][N], int min_col[N]) {

    for(int i = 0; i < N; i++)
        min_col[i] = INF;

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if (reduced_matrix[i][j] < min_col[j])
                min_col[j] = reduced_matrix[i][j];

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if (reduced_matrix[i][j] != INF && min_col[j] != INF)
                reduced_matrix[i][j] -= min_col[j];
}

int cost(int reduced_matrix[N][N]) {

    int cost = 0;

    int min_row[N];
    row_reduce(reduced_matrix, min_row);

    int min_col[N];
    column_reduce(reduced_matrix, min_col);

    for (int i = 0; i < N; i++) {

        if (min_row[i] != INT_MAX)
            cost += min_row[i];
        else
            cost += 0;
        if (min_col[i] != INT_MAX)
            cost += min_col[i];
        else
            cost += 0;
    }

    return cost;
}

Node* new_node(int parent_matrix[N][N], vector <pair <int, int> > path, int visited, int i, int j) {

    Node* node = new Node;
    node->path = path;

     for(int i = 0; i < N; i++)
        for(int j = 0; j < N; j++)
            node->reduced_matrix[i][j] = parent_matrix[i][j];

    if (visited != 0) {

        node->path.push_back(make_pair(i, j));

        for (int k = 0; k < N; k++) {
            node->reduced_matrix[i][k] = INF;
            node->reduced_matrix[k][j] = INF;
        }
    }

    node->reduced_matrix[j][0] = INF;

    node->visited = visited;
    node->current = j;

    return node;
}

void tsp(int cost_matrix[N][N]) {

    auto cmp = [](Node* left, Node* right) { return left->cost > right->cost; };
    priority_queue<Node*, vector<Node*>, decltype(cmp)> pq(cmp);

    vector<pair<int, int>> v;

    Node* root = new_node(cost_matrix, v, 0, INF, 0);
    root->cost = cost(root->reduced_matrix);
    pq.push(root);

    while (!pq.empty()) {

        Node* min = pq.top();
        pq.pop();

        int i = min->current;

        if (min->visited == N - 1) {

            min->path.push_back(make_pair(i, 0));

            for (int i = 0; i < min->path.size(); i++)
                cout << min->path[i].first + 1 << " —> " << min->path[i].second + 1 << endl;

            cout << min->cost << endl;
            return;
        }

        for (int j = 0; j < N; j++)
            if (min->reduced_matrix[i][j] != INF) {
                Node* child = new_node(min->reduced_matrix, min->path, min->visited + 1, i, j);
                child->cost = min->cost + min->reduced_matrix[i][j]+ cost(child->reduced_matrix);
                pq.push(child);
            }
        delete min;
    }
}


int main() {

    int cost_matrix[N][N];

    freopen("graphInputFile", "r", stdin);
    int edges;
    cin >> edges;
    for (int i = 0; i < edges; i++) {
        int x, y, c;
        cin >> x >> y >> c;
        cost_matrix[x][y] = c;
    }

    tsp(cost_matrix);

    return 0;
}
