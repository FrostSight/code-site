#include <stdio.h>
#include <conio.h>
int a[10][10], n, cost = 0;
bool visited[10];

int least(int row)
{
    int i, nc = 999;
    int min = 999, kmin;
    for (i = 0; i < n; i++)
    {
        if ((a[row][i] != 0) && (visited[i] == false))
            if (a[row][i] < min)
            {
                min = a[i][0] + a[row][i];
                kmin = a[row][i];
                nc = i;
            }
    }

    if (min != 999)
        cost += kmin;

    return nc;
}

void mincost(int city)
{
    int i, ncity;
    visited[city] = true;

    printf("%d to ", city + 1);

    ncity = least(city);

    if (ncity == 999)
    {
        ncity = 0;
        printf("%d", ncity + 1);
        cost += a[city][ncity];

        return;
    }
    mincost(ncity);
}

int main()
{
    int i, j;
    printf("Enter No. of Cities: ");
    scanf("%d", &n);

    printf("Enter Cost Matrix...... \n");
    for (i = 0; i < n; i++)
    {
        printf("Enter Elements of Row  : %d\n", i + 1);
        for (j = 0; j < n; j++)
        {
            scanf("%d", &a[i][j]);
        }
        visited[i] = false;
    }

    printf("\nThe cost list is below");
    for (i = 0; i < n; i++)
    {
        printf("\n");
        for (j = 0; j < n; j++)
        {
            printf("\t%d", a[i][j]);
        }
    }

    printf("\n\nThe Path is:\n");

    mincost(0);

    printf("\n\nMinimum cost:");
    printf("%d", cost);

    getch();
}
