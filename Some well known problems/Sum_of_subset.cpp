#include <bits/stdc++.h>
using namespace std;

int targetSum, totalNumbers;
int dp[12][12];

int chill(int targetSum, int totalNumbers, int numbers[])
{
    if (totalNumbers == -1) // if no array of numbers or array is finished
    {
        if (targetSum == 0)
            return 1;

        return 0;
    }
    if (targetSum < 0)
        return 0; // summation of positive numbers can never be negative

    if (targetSum == 0)
        return 1;

    // memoization
    if (dp[totalNumbers][targetSum] != -1)
        return dp[totalNumbers][targetSum];

    dp[totalNumbers][targetSum] = chill(targetSum - numbers[totalNumbers], totalNumbers - 1, numbers) || chill(targetSum, totalNumbers - 1, numbers); // picked || not picked
}

int main()
{
    memset(dp, -1, sizeof(dp));

    printf("Enter the target sum ");
    scanf("%d", &targetSum);
    printf("Enter total numbers ");
    scanf("%d", &totalNumbers);

    int numbers[totalNumbers];
    printf("Enter the numbers ");
    for (int i = 0; i < totalNumbers; i++)
        scanf("%d", &numbers[i]);

    cout << chill(targetSum, totalNumbers, numbers) << endl;
}
