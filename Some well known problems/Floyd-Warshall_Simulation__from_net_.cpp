#include <bits/stdc++.h>
using namespace std;

#define INF INT_MAX
vector<int> nodes[100];
vector<int> weights[100];

int D[100][100] = {0};
int P[100][100] = {-1};

void FLOYD(int n)
{
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= n; ++j)
        {
            if (i == j)
            {
                continue;
            }
            D[i][j] = INF;
        }
    }
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 0; j < nodes[i].size(); ++j)
        {
            D[i][nodes[i][j]] = weights[i][j];
            P[i][nodes[i][j]] = i;
        }
    }
    cout << "----------" << 0 << "-----------" << endl;
    cout << "D(" << 0 << "):" << endl;
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= n; ++j)
        {
            if (D[i][j] == INF)
            {
                cout << "INF ";
                continue;
            }
            cout << D[i][j] << ' ';
        }
        cout << endl;
    }
    cout << endl;
    cout << "P(" << 0 << "):" << endl;
    for (int i = 1; i <= n; ++i)
    {
        for (int j = 1; j <= n; ++j)
        {
            if (P[i][j] == 0)
            {
                cout << "NIL ";
                continue;
            }
            cout << P[i][j] << ' ';
        }
        cout << endl;
    }
    cout << endl;

    for (int k = 1; k <= n; ++k)
    {
        cout << "----------" << k << "-----------" << endl;
        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (D[i][k] != INF && D[k][j] != INF && D[i][j] > D[i][k] + D[k][j])
                {
                    D[i][j] = D[i][k] + D[k][j];
                    P[i][j] = P[k][j];
                }
            }
        }
        cout << "D(" << k << "):" << endl;
        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (D[i][j] == INF)
                {
                    cout << "INF ";
                    continue;
                }
                cout << D[i][j] << ' ';
            }
            cout << endl;
        }
        cout << endl;
        cout << "P(" << k << "):" << endl;
        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (P[i][j] == 0)
                {
                    cout << "NIL ";
                    continue;
                }
                cout << P[i][j] << ' ';
            }
            cout << endl;
        }
        cout << endl;
    }
}

int main()
{
    int node, edge;
    cin >> node >> edge;
    for (int i = 0; i < edge; ++i)
    {
        int u, v, c;
        cin >> u >> v >> c;
        nodes[u].push_back(v);
        weights[u].push_back(c);
    }
    FLOYD(node);
}

/*
Input:
5 9
1 5 -4
5 4 6
4 3 -5
3 2 4
1 2 3
1 3 8
2 5 7
4 1 2
2 4 1

*/