#include <stdio.h>
#include <string.h>
int main()
{
    char s[10]; // index number has to be specified and data type is "char"
    printf("Enter string ");
    scanf("%s", &s); // format specifier is "%s" not "%c"

    int len = strlen(s); // not s.length()
    printf("Length : %d\n", len);
}