#include <bits/stdc++.h>
using namespace std;

int main() 
{
    vector<int> vec1 = {3, 2, 1};
    vector<int> vec2 = {10, 20, 30};

    vector<pair<int, int>> vp;

    for (size_t i = 0; i < vec1.size(); ++i) 
    {
        vp.push_back(make_pair(vec1[i], vec2[i]));
    }

    sort(vp.begin(), vp.end());

    for (auto& pair : vp) 
    {
        cout << "(" << pair.first << ", " << pair.second << ")" << endl;
    }

}


/*

Output:

(1, 30)
(2, 20)
(3, 10)

*/