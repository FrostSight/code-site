#include <bits/stdc++.h>
using namespace std;

int main()
{
    int arr[7] = {4, 8, 2, 7, 3, 6, 1};
    int n = 7;

    vector<int> v1(n, -1), v2(n, -1);

    v1[n - 1] = arr[n - 1];
    v2[n - 1] = n - 1;

    int maxValue = arr[n - 1];
    int maxCarryingIndex = n - 1;

    for (int i = n - 2; i >= 0; i--)
    {
        if (arr[i] >= maxValue)
        {
            maxValue = arr[i];
            maxCarryingIndex = i;
        }
        v1[i] = maxValue;
        v2[i] = maxCarryingIndex;
    }

    for (int i = 0; i < v1.size(); i++)
    {
        printf("%d ", v1[i]);
    }

    printf("\n");

    for (int i = 0; i < v2.size(); i++)
    {
        printf("%d ", v2[i]);
    }

    return 0;
}


/*
output
8 8 7 7 6 6 1 -> v1
1 1 3 3 5 5 6 -> v2
*/