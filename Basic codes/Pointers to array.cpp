#include <iostream>
using namespace std;

int main() 
{
    int arr[10] = {2,5,6, 78, 2, 45, 17, 30, 7, 55};
    printf("\n");
    cout << "At first ";
    for(int i = 0; i < 10; i++)
        cout<<arr[i]<<" ";
    printf("\n");
    
    cout << "Address of first block " << arr << endl; 
    cout << "Address of first block " << &arr[0] << endl; 
    cout << "Address of first block " << &arr<< endl;
    
    cout << "First block value " << arr[0] << endl; 
    cout << "First block value " << *arr << endl; 
    
    cout << "First block value increment " << *arr+1 << endl; 
    cout << "First block value increment " << *(arr)+1 << endl;
    
    cout << "Next block value " << *(arr+1) << endl; 
    
    printf("\n");
    cout << "At end ";
    for(int i = 0; i < 10; i++)
        cout<<arr[i]<<" ";
    printf("\n");
    
    
    
    
    int temp [10] = {1,2};
    cout << sizeof(temp) << endl;
    cout << "first value " << sizeof(*temp) << endl;
    
    int *p = &temp[0];
    cout << sizeof(p) << endl;
    cout << sizeof(*p) << endl;
    cout << sizeof(&p) << endl;
    
}

/*
output
At first 2 5 6 78 2 45 17 30 7 55 
Address of first block 0x7ffe70531780
Address of first block 0x7ffe70531780
Address of first block 0x7ffe70531780
First block value 2
First block value 2
First block value increment 3
First block value increment 3
Next block value 5
At end 2 5 6 78 2 45 17 30 7 55 
40
first value 4
8
4
8
*/
