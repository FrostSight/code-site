#include <bits/stdc++.h>
using namespace std;

int main()
{
    vector<int> arr = {-2, -1, 4, 1, -2}, result(5, 0);
    int n = 5;

    for(int i = 0; i < n; i++)
    {
        if(arr[i] > 0)
        {
            int idx = (i + arr[i]) % n;
            result[idx] = arr[i];
        }
        else
        {
            int idx = (i - abs(arr[i])) % n;
            if (idx < 0)  // boundary check
                idx += n;
            result[idx] = arr[i];
        }
    }

    for(int i = 0; i < n; i++)
        cout << result[i] << " ";
}


/*
Circular left and right shift
*/