#include <bits/stdc++.h>
using namespace std;

bool sortBySecond(pair<int, int>& a, pair<int, int>& b) 
{
    return a.second < b.second;
}

int main() 
{
    vector<int> vec1 = {3, 2, 1};
    vector<int> vec2 = {10, 20, 30};

    vector<pair<int, int>> vp;

    for (size_t i = 0; i < vec1.size(); ++i) 
    {
        vp.push_back(make_pair(vec1[i], vec2[i]));
    }

    sort(vp.begin(), vp.end(), sortBySecond);

    for (auto& pair : vp) 
    {
        cout << "(" << pair.first << ", " << pair.second << ")" << endl;
    }

}


/*

Output:

(3, 10)
(2, 20)
(1, 30)

*/