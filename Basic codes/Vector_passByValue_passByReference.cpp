#include <bits/stdc++.h>
using namespace std;

int fun2(vector<int>& v) // pass by reference
{
  int sum = 0;

  for(int i = 0; i < 3; i++)
    sum += v[i];

    return sum;
}

int fun(vector<int> v) // pass by values
{
  int sum = 0;

  for(int i = 0; i < 3; i++)
    sum += v[i];

    return sum;
}

int main()
{
    vector<int> v = {2,3,4};
    cout << fun(v) << endl;

    cout << fun2(v) << endl;
}

/*
in pass by reference
- working / manipulating vector itself

in pass by values
- creates a copy of  vector
- takes O(n) time to copy
*/
