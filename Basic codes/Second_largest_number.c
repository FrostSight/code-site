#include <stdio.h>
#include <limits.h>
int main()
{
    int n, l, s;
    printf("Enter total numbers ");
    scanf("%d", &n);

    int arr[n];
    printf("Enter numbers ");
    for (int i = 0; i < n; i++)
        scanf("%d", &arr[i]);

    l = s = INT_MIN; // l stores the largest and s stores the 2nd largest
    for (int i = 0; i < n; i++)
    {
        if (arr[i] > l)
        {
            s = l;
            l = arr[i];
        }
        else if (arr[i] > s && arr[i] != l) // less than l but greater than greater than the previous value of s
        {
            s = arr[i];
        }
    }
    if (s == INT_MIN)
        printf("There is no second largest number\n");
    else
        printf("There second largest number is %d\n", s);
}
