#method 1 --> empty placeholders
name = input ("Enter name ")
age = int(input("Enter age "))

print("My name is {} and I am {} years old".format(name, age))

#method 2 --> numbered index
print("My name is {0} and I am {1} years old".format("John", 10))

#method 3 --> named index
print("My name is {name} and I am {age} years old".format(name = "John", age = 15))
