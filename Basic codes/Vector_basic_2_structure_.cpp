#include<bits/stdc++.h>
using namespace std;

struct Person
{
    string gender;
    int age;
}person1; // global declaration

void display(vector <Person> vec)
{
    printf("Size : %d\n", vec.size());

    // method 1
    for(int i=0; i<vec.size(); i++)
    {
        //printf("%s %d", vec[i].gender, vec[i].age); // don't why name doesn't get printed
        cout<<vec[i].gender<<' '<<vec[i].age<<endl;
    }
    cout<<endl;

    // method 2
    for(auto it : vec)
    {
        cout<<it.gender<<" "<<it.age<<endl;
    }
    cout<<endl;
}

int main()
{
    person1 = {"Female", 39};
    Person person2 = {"Male", 35}; // local declaration

    vector <Person> vec;
    vec.push_back(person1);
    vec.push_back(person2);

    display(vec);
}

