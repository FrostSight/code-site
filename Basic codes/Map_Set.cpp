#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n;
    printf("Enter array size: ");
    scanf("%d", &n);

    int arr[n];

    printf("Enter elements ");
    for (int i = 0; i < n; i++)
        scanf("%d", &arr[i]);
    
    printf("Array elements are: ");
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);

    map <int, int> counter;
    for (int i = 0; i < n; i++)
    {
        counter[arr[i]]++;
    }

    printf("\n\nKEY  VALUE\n");
    for(auto it = counter.begin(); it != counter.end(); it++)
    {
        cout << it->first << " " << it->second <<endl;
    }

    set <int> s;
    for(auto it = counter.begin(); it != counter.end(); it++)
    {
        s.insert(it->second);
    }

    printf("\nUnique values are\n");
    for(auto it = s.begin(); it != s.end(); it++)
    {
        cout << *it <<endl;
    }

    if (counter.size() == s.size())
        printf("\nThere are no repeated values\n");
    else
        printf("\nThere are repeated values\n");
}
