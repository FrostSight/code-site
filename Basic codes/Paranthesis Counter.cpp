#include <bits/stdc++.h>
using namespace std;

int main() 
{
    string s = "[{()}(){()}{}[][{}]]";
    map<char, int> mp;
    
    for(char& ch : s)
    {
      char p = ch;
      
      if(ch == ')')
        p = '(';
      else if(ch == '}')
        p = '{';
      if(ch == ']')
        p = '[';
      
      mp[p]++;
    }
    
    for(auto it = mp.begin(); it != mp.end(); it++)
      cout << it->first << " " << it->second << endl;
}

/*
output
( 6
[ 6
{ 8
*/