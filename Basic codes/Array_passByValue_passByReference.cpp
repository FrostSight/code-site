#include <bits/stdc++.h>
using namespace std;

int fun2(int *arr) // pass by reference
{
  int sum = 0;

  for(int i = 0; i < 3; i++)
    sum += arr[i];

    return sum;
}

int fun(int arr[]) // pass by values
{
  int sum = 0;

  for(int i = 0; i < 3; i++)
    sum += arr[i];

    return sum;
}

int main()
{
    int arr[10] = {2,3,4};
    cout << fun(arr) << endl;

    cout << fun2(arr) << endl;
}

/*
in pass by reference
- working / manipulating vector itself

in pass by values
- creates a copy of  vector
- takes O(n) time to copy
*/


void fun1(vector<int> a) // pass by value
{
  a[3] = 99;
  cout << "\nfun1" << endl;
  for(int i = 0; i < a.size(); i++)
    cout << a[i] << " ";
}


void fun2(vector<int>& a) // pass by reference
{
  a[3] = 999;
  cout << "\nfun2" << endl;
  for(int i = 0; i < a.size(); i++)
    cout << a[i] << " ";
}




/*
int main() 
{
    vector<int> nums = {1,2,3,4};
    
    cout << "\nbefore" << endl;
    for(int i = 0; i < nums.size(); i++)
    cout << nums[i] << " ";
    
    fun1(nums);
    fun2(nums);
    
    
    
    cout << "\nafter" << endl;
    for(int i = 0; i < nums.size(); i++)
    cout << nums[i] << " ";
    
    
}
*/
