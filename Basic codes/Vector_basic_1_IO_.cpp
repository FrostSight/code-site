#include<bits/stdc++.h>
using namespace std;

void display(vector <int> vec)
{
    printf("Size : %d\n", vec.size());

    // method 1
    for(int i=0; i<vec.size(); i++)
    {
        printf("%d ", vec[i]);
    }
    cout<<endl;

    // method 2
    for(auto it : vec)
    {
        printf("%d ", it);
    }
    cout<<endl;
}

int main()
{
    vector <int> vec;
    vec.push_back(10);
    vec.push_back(113);
    vec.push_back(21);
    vec.push_back(35);
    vec.push_back(19);

    display(vec);
}
