#include<bits/stdc++.h>
using namespace std;

int main()
{
    vector<char> arr = {'U', 'V', 'W', 'X', 'Y', 'Z'};
    int n = arr.size(), times = 3;
    int totalTime = n *  times;

    for(int i = 0; i < totalTime; i++)
    {
        cout << arr[i % n] <<  " ";
    }
}

/*
Main formula: i % n
*/