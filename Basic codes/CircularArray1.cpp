#include<bits/stdc++.h>
using namespace std;

void revolving(vector<char>& arr, int idx)
{
    int n = arr.size();
    for(int i = idx; i < n+idx; i++)
    {
        cout << arr[i % n] << " ";
    }
}

int main()
{
    vector<char> arr = {'D', 'E', 'F', 'G', 'H'};
    int startingIndex = 3;

    revolving(arr, startingIndex);
}


/*
This code is about revolving around a single index.
*/