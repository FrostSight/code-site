// it is an undirected and connected graph

#include <bits/stdc++.h>
using namespace std; //it is important beacause of the STL used

vector<int> graph[5];
bool checked[5]; // initializing array as 'false' value means they are not visited yet

void traversing(int startingNode)
{
    checked[startingNode] = true;                        // now the starting node has visited so it becomes true
    for (int i = 0; i < graph[startingNode].size(); i++) // traversing through the sub indexes
    {
        int adjacentNode = graph[startingNode][i];
        if (checked[adjacentNode] == false) // checking if the adjacency node if the starting node is visited or not
            traversing(adjacentNode);       //recursive calling
    }
}

int main()
{
    int node, edge;
    printf("Number of nodes ");
    scanf("%d", &node);

    printf("Number of edges ");
    scanf("%d", &edge);

    printf("Enter nodes\n");
    for (int i = 0; i < edge; i++)
    {
        int u, v;
        cin >> u >> v; //taking input as adjucency list
        graph[u].push_back(v);
        graph[v].push_back(u); // as it is an undirected graph
    }

    /*for (int i=0; i<node; i++)
    {
        if(checked[i] == false)
            traversing(i);
    }*/

    int tartingNode; //go to the line 9
    printf("Enter a source node to start traversing ");
    scanf("%d", &tartingNode);

    traversing(tartingNode);

    for (int i = 0; i < node; i++) //displaying nodes
    {
        if (checked[i] == true)
            printf("%d node is visited\n", i);
    }
}

/*As it is an undirected and CONNECTED graph, we just need any a single node to start traversing and it will automatically traverse
other nodes, we don't need to worry about calling or checking other nodes manually.

But if there is a DISCONNECTED graph we must call or check other nodes manually to do a complete traverse. We can do it by usuing a
for loop. Check the line 38, i case of disconnected just remove the comment syntex.*/
