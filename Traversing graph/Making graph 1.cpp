#include <bits/stdc++.h>
using namespace std;

int main()
{
    vector<vector<int>> edges = {{0,3},{0,4},{1,3},{2,4},{2,7},{3,5},{3,6},{3,7},{4,6}};
    int n = 8;
    vector<vector<int>> adj(n);

    for(vector<int>& edge : edges)
    {
        int u = edge[0];
        int v = edge[1];

        adj[u].push_back(v);
    }

    for(int u = 0; u < n; u++)
    {
        cout << u << ": ";
        for(int v : adj[u])
            cout << v << " ";
        cout << endl;
    }

}

