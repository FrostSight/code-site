#include<bits/stdc++.h>
using namespace std;

void traversal(vector<vector<int>>& graph, int u, vector<bool>& visited)
{
    if(visited[u] == true)  return;

    cout << u << " ";
    visited[u] = true;

    for(int v : graph[u])
    {
        if(visited[v] == false)
            traversal(graph, v, visited);
    }
}

int main()
{
    // scattered edges
    vector<vector<int>> edges = {
        {0, 2},
        {2, 4},
        {0, 3},
        {0, 1} };

    vector<vector<int>> graph(5);

    // making the graph
    for(vector<int>& edge : edges)
    {
        int u = edge[0];
        int v = edge[1];

        graph[u].push_back(v);
    }

    // printing graphs
    for(int u = 0; u < 5; u++)
    {
        cout << u << ": ";
        for(int v : graph[u])
            cout << v << " ";

        cout << endl;
    }

    int startingNode = 0;
    vector<bool> visited(5, false); // 5 nodes 0, 1, 2, 3, 4
    printf("\nNodes in DFS order: ");
    traversal(graph, startingNode, visited);

}
