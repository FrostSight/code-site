#include<bits/stdc++.h>
using namespace std;

bool dfsCycleDetection(vector<vector<int>>& graph, int u, vector<bool>& visited, vector<bool>& inRecStack, vector<int>& parent, vector<int>& cycle)
{
    visited[u] = true;
    inRecStack[u] = true;

    for(int v : graph[u])
    {
        if(visited[v] == false) // If v is not visited, continue DFS
        {
            parent[v] = u;
            if(dfsCycleDetection(graph, v, visited, inRecStack, parent, cycle))
                return true;
        }
        else if(inRecStack[v] && v != parent[u]) // If v is in the recursion stack, we found a cycle
        {
            // To record the cycle path
            int current = u;
            cycle.push_back(v);
            while (current != v)
            {
                cycle.push_back(current);
                current = parent[current];
            }
            cycle.push_back(v);
            reverse(cycle.begin(), cycle.end()); // to show the cycle in correct order

            return true;
        }
    }

    inRecStack[u] = false;

    return false;
}

int main()
{
    // Given edges forming a graph
    vector<vector<int>> edges = { {0, 1}, {1, 2}, {2, 3}, {3, 4}, {4, 1} };
    vector<vector<int>> graph(5);

    // Creating the graph from edges
    for(vector<int>& edge : edges)
    {
        int u = edge[0];
        int v = edge[1];
        graph[u].push_back(v);
    }

    vector<bool> visited(5, false), inRecStack(5, false);
    vector<int> parent(5, -1), cycle;
    bool hasCycle = false;

    // Try to detect cycle starting from any node
    for(int i = 0; i < 5; i++)
    {
        if (visited[i] == false)
        {
            if(dfsCycleDetection(graph, i, visited, inRecStack, parent, cycle))
            {
                hasCycle = true;
                break;
            }
        }
    }

    // Output result
    if (hasCycle == true)
    {
        cout << "Cycle detected: ";
        for (int node : cycle)
        {
            cout << node << " ";
        }
    }
    else
        cout << "No cycle exists" << endl;

}

/*
Cycle detection with path in Undirected graph using DFS.
*/