#include <bits/stdc++.h>

struct Node
{
    int data;
    struct Node *rkid, *lkid;
};
struct Node *root;

void preodering(struct Node *root) // root -> left -> right
{
    if (root == NULL)
        return;
    else
    {
        printf("%d ", root->data);
        preodering(root->lkid);
        preodering(root->rkid);
    }
}

void inserting(int k)
{
    struct Node *temp1 = (struct Node *)malloc(sizeof(struct Node));
    temp1->data = k;
    temp1->lkid = NULL;
    temp1->rkid = NULL;

    if (root == NULL)
        root = temp1;
    else
    {
        struct Node *temp2 = root;
        while (true)
        {
            if (temp1->data <= temp2->data) //traversing left kid
            {
                if (temp2->lkid == NULL)
                {
                    temp2->lkid = temp1;
                    break;
                }
                else
                    temp2 = temp2->lkid;
            }
            else //traversing right kid
            {
                if (temp2->rkid == NULL)
                {
                    temp2->rkid = temp1;
                    break;
                }
                else
                    temp2 = temp2->rkid;
            }
        }
    }
}

int main()
{
    root = NULL;

    srand(time(0)); // Seed the random number generator
    for (int i = 0; i < 10; ++i)
    {
        int random_digit = rand() % 10; // Generate a random digit (0 to 9)
        inserting(random_digit);

    }

    printf("\n");

    preodering(root);
}
