#include<bits/stdc++.h>
using namespace std;

bool dfsCycleDetection(vector<vector<int>>& graph, int u, vector<bool>& visited, vector<bool>& inRecStack)
{
    visited[u] = true;
    inRecStack[u] = true;

    for(int v : graph[u])
    {
        if(visited[v] == false) // If v is not visited, continue DFS
        {
            if(dfsCycleDetection(graph, v, visited, inRecStack))
                return true;
        }
        else if(inRecStack[v])
            return true;
    }

    inRecStack[u] = false;

    return false;
}

int main()
{
    // Given edges forming a graph
    vector<vector<int>> edges = { {0, 1}, {1, 2}, {2, 3}, {3, 4}, {4, 1} };
    vector<vector<int>> graph(5);

    // Creating the graph from edges
    for(vector<int>& edge : edges)
    {
        int u = edge[0];
        int v = edge[1];

        graph[u].push_back(v);
    }

    vector<bool> visited(5, false), inRecStack(5, false);
    bool hasCycle = false;

    for(int i = 0; i < 5; i++)
    {
        if (visited[i] == false)
        {
            if(dfsCycleDetection(graph, i, visited, inRecStack))
            {
                hasCycle = true;
                break;
            }
        }
    }

    // Output result
    hasCycle == true ? cout << "Found" << endl : cout << "Not Found" << endl;

}

/*
Cycle detection without path in Undirected graph using DFS
*/