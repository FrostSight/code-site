#include <bits/stdc++.h>
using namespace std; //it is important because of the STL used

int dist[10];     // by default it is initialized with 0 for all indexses
bool visited[10]; // initializing (by default) array as 'false' value means they are not visited yet
vector<int> graph[10];

void traversing(int startingNode)
{
    dist[startingNode] = 0;
    visited[startingNode] = true;

    queue<int> q;
    q.push(startingNode);

    while (!q.empty())
    {
        int x = q.front();
        q.pop();

        for (int i = 0; i < graph[x].size(); i++)
        {
            int node = graph[x][i];
            if (visited[node] == false)
            {
                visited[node] = true;
                dist[node] = dist[x] + 1; // distance[v] = distance [u] + cost(uv) it is the actual formula. Here I consider the cost is constant and it is 1 for all edges
                q.push(node);
            }
        }
    }
}

int main()
{
    int node, edge;
    printf("Enter number of nodes ");
    scanf("%d", &node);
    printf("Enter number of edges ");
    scanf("%d", &edge);

    printf("Enter nodes\n");
    for (int i = 0; i < edge; i++)
    {
        int u, v;
        scanf("%d%d", &u, &v);
        graph[u].push_back(v);
        graph[v].push_back(u); // as it is an undirected graph
    }

    int source;
    printf("\nEnter node to start traversing ");
    scanf("%d", &source);
    traversing(source);

    printf("\n");
    for (int i = 1; i <= node; i++)
    {
        printf("From node %d distance of %d is %d\n", source, i, dist[i]);
    }
}
