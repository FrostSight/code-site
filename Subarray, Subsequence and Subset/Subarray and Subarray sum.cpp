#include <bits/stdc++.h>
using namespace std;

#define ar array
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define vi vector<int>
#define vs vector<string>
#define vll vector<long long>
#define mii map<int, int>
#define umii unordered_map<int, int>
#define umci unordered_map<char, int>
#define si set<int>
#define sc set<char>
#define ss set<string>

const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;


void findSubArraySum(vi& nums)
{
    vi subarraySum;

    printf("\nAll sums of all sub array is\n");

    for(int i = 0; i < nums.size(); i++)
    {
        int sum = 0;
        for(int j = i; j < nums.size(); j++)
        {
            sum += nums[j];
            subarraySum.push_back(sum);
        }
    }

    for(int i = 0; i < subarraySum.size(); i++)
        cout << subarraySum[i] << " ";
}

void findSubArray(vi& nums)
{
    printf("\nSub array is\n");

    for(int i = 0; i < nums.size(); i++)
    {
        for(int j = i; j < nums.size(); j++)
        {
            for(int k = i; k <= j; k++)
            {
                cout << nums[k] << " ";
            }
            cout << endl;
        }
    }
}

void solve()
{
    vi nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    findSubArray(nums);

    findSubArraySum(nums);
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++)
    {
        // cout << "Case #" << t << ": ";
        solve();
    }
}
