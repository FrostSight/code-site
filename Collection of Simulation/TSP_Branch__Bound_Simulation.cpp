#include <iostream>
#include <vector>
#include <queue>
using namespace std;

#define INT_MAX 2147483647

const int row = 4;
const int col = 4;

class node {
public:
    int name, cost = 0;
    int arr[row][col];
    vector<int> path;
    
    void print() {
        int t = path.back();
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                if(i == t && j == 0) {
                    cout << "INF ";
                }
                else if (arr[i][j] == INT_MAX) {
                    cout << "INF ";
                }
                else {
                    cout << arr[i][j] << ' ';
                }
            }
            cout << endl;
        }
    }

    void copy(int a[row][col]) {
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                arr[i][j] = a[i][j];
            }
        }
    }

    void reduce_array(int from, int to, int a[row][col]) {
        copy(a);
        arr[to][from] = INT_MAX;
        for (int i = 0; i < row; ++i) {
            arr[from][i] = INT_MAX;
            arr[i][to] = INT_MAX;
        }
    }

    void calculate_cost(int prev_cost, int edge_cost) {
        int rowCost = 0, colCost = 0;
        for (int i = 0; i < row; ++i) {
            int mn = INT_MAX;
            for (int j = 0; j < col; ++j) {
                mn = min(arr[i][j], mn);
            }
            if (mn == INT_MAX) {
                mn = 0;
            }
            rowCost += mn;
            for (int j = 0; j < col; ++j) {
                if (arr[i][j] != INT_MAX) {
                    arr[i][j] -= mn;
                }
            }
        }
        cout << "\nAfter row reduction:\n";
        print();
        for (int i = 0; i < col; ++i) {
            int mn = INT_MAX;
            for (int j = 0; j < row; ++j) {
                mn = min(arr[j][i], mn);
            }
            if (mn == INT_MAX) {
                mn = 0;
            }
            colCost += mn;
            for (int j = 0; j < row; ++j) {
                if (arr[j][i] != INT_MAX) {
                    arr[j][i] -= mn;
                }
            }
        }
        cout << "\nAfter col reduction:\n";
        print();
        cout << "Column Reduction Cost: " << colCost << endl << "Row Reduction Cost: " << rowCost << endl;
        cout << "Previous Reduced Cost: " << prev_cost << endl << "Edge Cost: " << edge_cost << endl;
        cost = prev_cost + edge_cost + colCost + rowCost;
        cout << "Total Cost: " << cost << endl;
    }
};

bool operator < (const node& a, const node& b) {
        return a.cost > b.cost;
}

priority_queue<node> pq;

int main() {
    int arr[row][col] = {
        {INT_MAX, 10, 15, 20},
        {5, INT_MAX, 9, 10},
        {6, 13, INT_MAX, 12},
        {8, 8, 9, INT_MAX}
    };
    node t, res;
    t.name = 0;
    t.copy(arr);
    t.path.push_back(0);
    t.calculate_cost(0, 0);
    cout << "______________________________" << endl;
    cout << endl;
    pq.push(t);
    while (true) {
        node curr = pq.top();
        pq.pop();
        if (curr.path.size() > row) {
            res = curr;
            break;
        }
        for (int i = 0; i < row; ++i) {
            if(curr.arr[curr.name][i] == INT_MAX) {
                continue;
            }
            if(curr.path.size() != col && i == 0) {
                continue;
            }
            node temp;
            temp.name = i;
            temp.path = curr.path;
            temp.path.push_back(i);
            cout << "Current path:";
            for(auto i : temp.path) {
                cout << ' ' << i + 1;
            }
            temp.reduce_array(curr.name, i, curr.arr);
            cout << "\nCurrent Matrix:\n";
            temp.print();
            temp.calculate_cost(curr.cost, curr.arr[curr.name][i]);
            pq.push(temp);
            //cout << "\nFinal Matrix:\n";
            //temp.print();
            cout << "______________________________" << endl;
        }
    }
    cout << "Path: ";
    for (auto i : res.path) {
        cout << i + 1<< ' ';
    }
    cout << endl << "Cost: " << res.cost << endl;
}

/*
{INT_MAX, 20, 30, 10, 11},
{15, INT_MAX, 16, 4, 2},
{3, 5, INT_MAX, 2, 4},
{19, 6, 18, INT_MAX, 3},
{16, 4, 7, 16, INT_MAX}
INT_MAX, 10, 15, 20
5, INT_MAX, 9, 10
6, 13, INT_MAX, 12
8, 8, 9, INT_MAX

Message #gamers
*/