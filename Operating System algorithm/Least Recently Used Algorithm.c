#include<Stdio.h>
int main()
{
    int page_num, page_ref, frame_size, i, j, k, input, fault=0, hits=0, temp;

    printf("Enter the number of page references : ");
    scanf("%d",&page_ref);

    printf("Enter the page frame size : ");
    scanf("%d",&frame_size);

    int frame[frame_size], pages[page_ref];
    for(i=0; i<frame_size; i++)
        frame[i]=-1;

    printf("Enter the page references :\n");
    for(i=0; i<page_ref; i++)
    {
        scanf("%d",&pages[i]);
    }

    printf("Ref String\tPage Frame\n");
    for(i=0; i<page_ref; i++)
    {
        input=pages[i];

        for(j=0; j<frame_size; j++)
        {
            if(frame[j]==input)
            {
                if(j!=0)
                {
                    temp=frame[j];
                    for(k=j-1; k>=0; k--)
                    {
                        frame[k+1]=frame[k];
                    }
                    frame[0]=temp;
                }
                hits++;
                break;
            }
        }
        if(j<frame_size)
            continue;

        for(k=frame_size-1; k>=0; k--)
        {
            frame[k+1]=frame[k];
        }
        frame[0]=input;
        fault++;

        printf("%d\t\t",input);
        for(j=0; j<frame_size; j++)
        {
            printf("%d\t",frame[j]);
        }
        printf("\n");
    }

    printf("Page Fault : %d\n",fault);
    printf("Page Hits : %d\n",hits);
    printf("Page Fault Rate : %f\n\n\n",(float)fault*100.0/page_ref);

    return 0;
}
