#include<Stdio.h> 
int main() 
{ 
    int page_num, page_ref, frame_size, i, j, k, m, input, fault=-1, misses=1, hits=1, temp, count=0; 
 
    printf("Enter the number of page references : "); 
    scanf("%d",&page_ref); 
 
    printf("Enter the page frame size : "); 
    scanf("%d",&frame_size); 
 
    int frame[frame_size], pages[page_ref]; 
 
    for(i=0; i<frame_size; i++) 
        frame[i]=-1; 
 
    printf("Enter the page references :\n"); 
    for(i=0; i<page_ref; i++) 
    { 
        scanf("%d",&pages[i]); 
    } 
 
    printf("Ref String\tPage Frame\n"); 
    for(i=0; i<page_ref; i++) 
    { 
        input=pages[i]; 
 
        for(j=0; j<frame_size; j++) 
        { 
            if(frame[j]==input) 
            { 
                if(i>2) 
                    hits++; 
                printf("%d\n",frame[j]); 
                break; 
            } 
        } 
        if(j<frame_size) 
            continue; 
 
        for(j=i+1; j<page_ref; j++) 
        { 
            count=0; 
            for(k=0; k<frame_size; k++) 
            { 
                if(pages[j]==frame[k] && k != 0) 
                { 
                    temp=frame[k]; 
                    for(m=k-1; m>=0; m--) 
                    { 
                        frame[m+1]=frame[m]; 
                    } 
                    frame[0]=temp; 
                    count++; 
                    break; 
                } 
                else if(pages[j]==frame[k] && k==0) 
                { 
                    count++; 
                    break; 
                } 
            } 
            if(count==2) 
            { 
                break; 
            } 
        } 
 
        for(k=frame_size-1; k>=0; k--) 
        { 
            frame[k+1]=frame[k]; 
        } 
        frame[0]=input; 
        fault++; 
 
        printf("%d\t\t",input); 
        for(j=0; j<frame_size; j++) 
        { 
            printf("%d\t",frame[j]); 
        } 
        printf("\n"); 
    } 
 
    printf("\nPage Fault : %d\n", fault); 
    //printf("\nPage Misses : %d\n", misses); 
    printf("\nPage Hits : %d\n", hits); 
    printf("\nPage Fault : %f\n",(float)(fault)*100.0/(float)page_ref); 
 
    return 0; 
}
