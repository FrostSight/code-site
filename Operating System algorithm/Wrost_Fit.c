#include<stdio.h>
int hole_num, req_num, hole[15], hole_sort[15], hole_pos[15], req_size[15], hole_num_for_req[15], done[15];
int i,j, temp;

int Sort_the_Resources()
{
    for(i=0; i<hole_num; i++)
    {
        for(j=0; j<hole_num; j++)
        {
            if(hole_sort[i]>hole_sort[j])
            {
                temp=hole_sort[i];
                hole_sort[i]=hole_sort[j];
                hole_sort[j]=temp;

                temp=hole_pos[i];
                hole_pos[i]=hole_pos[j];
                hole_pos[j]=temp;

            }
        }
    }
}

int main()
{

    printf("Enter the number of holes : ");
    scanf("%d", &hole_num);

    printf("\nEnter the size of the holes : \n");
    for(i=0; i<hole_num; i++)
    {
        printf("Holes%d : ", i+1);
        scanf("%d", &hole[i]);
        hole_sort[i]=hole[i];
        hole_pos[i]=i;
    }

    printf("Enter the number of request : ");
    scanf("%d", &req_num);

    printf("\nEnter the size of the request : \n");
    for(i=0; i<req_num; i++)
    {
        printf("Request%d : ", i+1);
        scanf("%d", &req_size[i]);
        done[i]=0;
    }

    Sort_the_Resources();

    for(int k=0; k<req_num; k++)
    {
        for(j=0; j<hole_num; j++)
        {
            if(req_size[k]<=hole_sort[j])
            {
                hole_num_for_req[k]=hole_pos[j];
                done[k]=1;
                hole_sort[j]=hole_sort[j]-req_size[k];
                Sort_the_Resources();
                break;
            }
        }
    }

    printf("\nRequest No.\tRequest Size\tHole No.\tHole Size\tHole Rest\tAllocation\n");
    for(i=0; i<req_num; i++)
    {
        int position=hole_num_for_req[i];
        if(done[i])
        {
            printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\tYes\n", i+1, req_size[i], position+1, hole[position], hole[position]- req_size[i]);
            hole[position]=hole[position]-req_size[i];
        }
        else
            printf("%d\t\t%d\t\t\t\t\t\t\t\tNo\n", i+1, req_size[i]);
    }

    return 0;
}
