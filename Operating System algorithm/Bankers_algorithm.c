#include<stdio.h> 
 
int alloc[10][10], max[10][10], need[10][10], avail[1][10], pross_num, resour_num; 
int i, j; 
 
int check(int i) 
{ 
    for(int j=0; j<resour_num; j++) 
        if(avail[0][j]<need[i][j]) 
            return 0; 
 
    return 1; 
} 
 
void isSafe() 
{ 
 
    int com[5]={0,0,0,0,0}; 
    j=0; 
 
    while(1) 
    { 
        int allocated = 0; 
 
        for(int i=0; i<pross_num; i++) 
            if(!com[i] && check(i)) 
            { 
                for(int k=0; k<resour_num; k++) 
                    avail[0][k]=avail[0][k]-need[i][k]+max[i][k]; 
 
                    printf("Process %d\t",i); 
 
                allocated=com[i]=1; 
                j++; 
            } 
 
        if(!allocated) 
            break; 
    } 
    if(j==pross_num) 
        printf("\nSafely allocated"); 
    else 
        printf("\nAll processes can't be allocated safely"); 
} 
 
int main() 
{ 
    printf("Enter the number of processes : "); 
    scanf("%d", &pross_num); 
 
    printf("Enter the number of resources : "); 
    scanf("%d", &resour_num); 
 
    printf("Enter the Allocation matrix : \n"); 
    for(i=0; i<pross_num; i++) 
    { 
        for(j=0; j<resour_num; j++) 
            scanf("%d", &alloc[i][j]); 
        printf("\n"); 
    } 
 
    printf("Enter the Max matrix : \n"); 
    for(i=0; i<pross_num; i++) 
    { 
        for(j=0; j<resour_num; j++) 
        { 
            scanf("%d", &max[i][j]); 
            need[i][j] = max[i][j] - alloc[i][j]; 
        } 
 
        printf("\n"); 
    } 
 
    printf("Enter the available resources : \n"); 
    for(j=0; j<resour_num; j++) 
    { 
        scanf("%d", &avail[0][j]); 
    } 
 
    printf("\n"); 
 
    printf("The Need matrix : \n"); 
    for(i=0; i<pross_num; i++) 
    { 
        for(j=0; j<resour_num; j++) 
            printf("%d\t", need[i][j]); 
        printf("\n"); 
    } 
 
    isSafe(); 
 
    return 0; 
}